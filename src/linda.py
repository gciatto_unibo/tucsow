from typing import *
from lxml import etree as xml
import asyncio


class Linda:
    def put(self, *tuples: Any):
        raise NotImplementedError()

    def read(self, template: Any, limit: Optional[int] = None) -> Iterable[Any]:
        raise NotImplementedError()

    def take(self, template: Any, limit: Optional[int] = None) -> Iterable[Any]:
        raise NotImplementedError()


def _path_to_root(node: xml.Element) -> Iterable[xml.Element]:
    curr = node
    while curr.getparent() is not None:
        yield curr
        curr = curr.getparent()
    yield curr


def last(xs: Iterable[Any], default: Any = None) -> Any:
    l = default
    for x in xs:
        l = x
    return l


class XMLLinda(Linda):
    def __init__(self, tuple_center_name: str):
        self._tc_name = tuple_center_name
        self._root = xml.Element(tuple_center_name)
        self._root_tree = xml.ElementTree(self._root)
        self._pending_queries = dict()

    @staticmethod
    def _normalize_inputs(tuples: Iterable[Any]):
        def _normalize_one(t: Union[Any, str]):
            if isinstance(t, str):
                return xml.XML(t)
            else:
                return t

        return [_normalize_one(t) for t in tuples]

    @staticmethod
    def _normalize_outputs(nodes):
        return [xml.tostring(r, pretty_print=False, encoding="unicode") for r in nodes]

    def _remove(self, node):
        if last(_path_to_root(node)) is self._root:
            parent = node.getparent()
            parent.remove(node)

    async def put(self, *tuples: Union[xml.Element, str]):
        for t in XMLLinda._normalize_inputs(tuples):
            self._root.append(t)

        for template, event in self._pending_queries.items():
            results = self._root_tree.xpath(template)
            if len(results) > 0:
                event.set()

    async def _select(self, template: Any, limit: Optional[int]) -> Iterable[xml.Element]:
        results = self._root_tree.xpath(template)
        while len(results) == 0:
            matching_tuple_inserted = asyncio.Event()
            self._pending_queries[template] = matching_tuple_inserted
            await matching_tuple_inserted.wait()
            matching_tuple_inserted.wait()
            results = self._root_tree.xpath(template)

        actual_limit = limit if limit is not None else len(results)

        return results[:actual_limit]

    async def read(self, template: str, limit: Optional[int] = None) -> Iterable[str]:
        results = await self._select(template, limit)

        return XMLLinda._normalize_outputs(results)

    async def take(self, template: str, limit: Optional[int] = None) -> Iterable[str]:
        results = await self._select(template, limit)

        for node in results:
            self._remove(node)

        return XMLLinda._normalize_outputs(results)

    def __repr__(self) -> str:
        return xml.tostring(self._root, pretty_print=False, encoding="unicode")

    def __str__(self) -> str:
        return xml.tostring(self._root, pretty_print=True, encoding="unicode")