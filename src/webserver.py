from aiohttp import web
from routes import tuple_center_route


async def _handle_index(request):
    print(f"Request: {str(request)}")
    text = \
        '''
        <h1>TuCSoW</h1>
        <h2>Tuple Centers Spread over the Web</h2>
        <p>You're welcome!</p>
        '''
    return web.Response(text=text, content_type="text/html")


app = web.Application()
app.router.add_get('/', _handle_index)
tuple_center_route.install(app)

web.run_app(app, port=8080)
