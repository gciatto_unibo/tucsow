from aiohttp import web
from linda import XMLLinda
from typing import *


_tuple_centers: Dict[str, XMLLinda] = dict()


def _get_tuple_center(name: str) -> XMLLinda:
    if name not in _tuple_centers:
        _tuple_centers[name] = XMLLinda(name)
    return _tuple_centers[name]


async def _handle_get_invocation(request: web.Request):
    print(f"Request: {str(request)}")
    name = request.match_info.get('tuple_center', "default")
    if request.can_read_body:
        query = await request.text()
    else:
        query = f"/{name}"
    tc = _get_tuple_center(name)
    results = await tc.read(query)
    return web.Response(text="\n".join(results), content_type="application/xml")


async def _handle_delete_invocation(request: web.Request):
    print(f"Request: {str(request)}")
    name = request.match_info.get('tuple_center', "default")
    if request.can_read_body:
        query = await request.text()
    else:
        query = "/"
    tc = _get_tuple_center(name)
    results = await tc.take(query)
    return web.Response(text="\n".join(results), content_type="application/xml")


async def _handle_post_invocation(request):
    print(f"Request: {str(request)}")
    name = request.match_info.get('tuple_center', "default")
    if request.can_read_body:
        query = await request.text()
    else:
        raise NotImplementedError()
    tc = _get_tuple_center(name)
    await tc.put(query)
    return web.Response(status=201)


def install(app: web.Application):
    app.router.add_get('/{tuple_center}', _handle_get_invocation)
    app.router.add_get('/', _handle_get_invocation)
    app.router.add_post('/{tuple_center}', _handle_post_invocation)
    app.router.add_post('/', _handle_post_invocation)
    app.router.add_delete('/{tuple_center}', _handle_delete_invocation)
    app.router.add_delete('/', _handle_delete_invocation)
