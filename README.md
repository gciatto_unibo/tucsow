# TuCSoW: __Tu__ple __C__enters __S__pread __o__ver the __W__eb

## Requirements

The following packages are necessary to run TuCSoW:

0. Python 3.5+ ([download page here](https://www.python.org/downloads/))
    - You may also install it by means of your package manager
    - Verify your installation by running `python --version`
0. Pip ([installation instuctions here](https://pip.pypa.io/en/stable/installing/))
    - It should be already available after you install Python
    - Verify your installation by running `pip --version`
0. Virtualenv
    - Install it by running `pip install virtualenv` on a high-privileges shell

## Setup your environment

0. Clone this repository 
0. Move within the main directory (the one containing `src/`)
0. Run `virtual env .`
0. From now on, every time you need to work on this project, you'll need to run `Scripts/activate` when you begin and `Scripts/decactivate` once you've finished
0. Run `pip install -r requirements.txt`
0. Run `python src/webserver.py`
    - Enjoy :)
    
## Producers consumers example

0. From some shell
    - `curl -d "//some_work" -X GET http://localhost:8080/some_tc`
        - The client will wait for another client to POST some work to do

0. From another shell
    - `curl -d "<to_do_list><some_work/></to_do_list>" -X POST http://localhost:8080/some_tc`

0. From your browser
    - Navigate to [http://localhost:8080/some_tc](http://localhost:8080/some_tc) to see the current state of the tuple center
    
0. Now retry using DELETE instead of GET